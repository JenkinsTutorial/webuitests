﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeleniumFramework
{
    public class DriverManager
    {
        static void Main()
        {
        }
        private static IWebDriver _driver;

        public static IWebDriver GetDriver()
        {
            return _driver;
        }

        public static IWebDriver Driver
        {
            get
            {
                if (_driver == null)
                {
                    _driver = new ChromeDriver(@"c:\jenkins\tools\chromedriver");
                    _driver.Manage().Window.Maximize();
                }
                return _driver;
            }
        }

        private static string GetCurrentWebDriverPath()
        {
            var applicationPath = System.Reflection.Assembly.GetExecutingAssembly().Location;
            var driverPathExtract = applicationPath.IndexOf("bin", StringComparison.Ordinal);
            var driverPath = applicationPath.Substring(0, driverPathExtract);
            return driverPath;
        }

        public static void ResetDriver()
        {
            Driver.Dispose();
            _driver = null;
        }
    }
}
