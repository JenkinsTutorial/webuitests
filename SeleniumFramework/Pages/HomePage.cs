﻿using OpenQA.Selenium;
using System;
using System.Threading;

namespace SeleniumFramework.Pages
{
    public class HomePage
    {
        public IWebElement AddItemButton => DriverManager.Driver.FindElement(By.CssSelector("[id = 'items_add_new']"));

        public IWebElement ItemDescriptionField => DriverManager.Driver.FindElement(By.XPath("//*[@id='item_description']"));

        public IWebElement InsertButton => DriverManager.Driver.FindElement(By.CssSelector("[id = 'insert']"));

        public IWebElement BackButton => DriverManager.Driver.FindElement(By.CssSelector("[id = 'deselect']"));

        public IWebElement ItemsButton => DriverManager.Driver.FindElement(By.XPath("//*[@id='items-tile']/div/div/div[1]/a[1]"));

        public IWebElement Item => DriverManager.Driver.FindElement(By.CssSelector("[id = 'items-item_description-1']"));

        public IWebElement DeleteButton => DriverManager.Driver.FindElement(By.CssSelector("[id = 'delete']"));

        public void SignOut()
        {
            DriverManager.Driver.FindElement(By.CssSelector("[class = 'btn btn-danger navbar-btn hidden-xs']")).Click();
            Thread.Sleep(3 * 1000);
            DriverManager.Driver.FindElement(By.CssSelector("[class = 'btn btn-warning navbar-btn']")).Click();
            Thread.Sleep(3 * 1000);
        }
    }
}
