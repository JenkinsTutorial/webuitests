﻿using SeleniumFramework;
using SeleniumFramework.Pages;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading;
using System;

namespace WebUiTests
{ //
    [TestClass]
    public class UnitTest1
    {
        public static Login Login => _login ?? (_login = new Login());

        private static Login _login;

        public static HomePage HomePage => _homePage ?? (_homePage = new HomePage());

        private static HomePage _homePage;

        [TestCleanup]
        public void CleanUp()
        {
            DriverManager.ResetDriver();
        }

        [TestMethod]
        public void LoginTest()
        {
            var appHost = Environment.GetEnvironmentVariable("AppHost");
            var appEnvironment = Environment.GetEnvironmentVariable("AppEnvironment");
            Console.WriteLine($"Navigating to: {appHost}/{appEnvironment}");
            Login.Navigate($"http://{appHost}/{appEnvironment}");
            Thread.Sleep(3 * 1000);
            Login.SignIn("devopsuser", "1q2w3e");
            Thread.Sleep(3 * 1000);
        }

        [TestMethod]
        public void AddAndRemoveItem()
        {
            var appHost = Environment.GetEnvironmentVariable("AppHost");
            var appEnvironment = Environment.GetEnvironmentVariable("AppEnvironment");
            Console.WriteLine($"Navigating to: {appHost}/{appEnvironment}");
            Login.Navigate($"http://{appHost}/{appEnvironment}");
            //Login.Navigate($"http://192.168.10.128/dev");
            Thread.Sleep(3 * 1000);
            Login.SignIn("devopsuser", "1q2w3e");
            HomePage.SignOut();
            Thread.Sleep(3 * 1000);
        }
    }
}