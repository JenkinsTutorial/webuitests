﻿using OpenQA.Selenium;
using System;

namespace SeleniumFramework.Pages
{
    public class Login
    {
        public IWebElement UsernameField => DriverManager.Driver.FindElement(By.Id("username"));

        public IWebElement PasswordField => DriverManager.Driver.FindElement(By.Id("password"));

        public IWebElement SubmitButton => DriverManager.Driver.FindElement(By.Id("submit"));


        
        public void Navigate(string url)
        {
            DriverManager.Driver.Navigate().GoToUrl(url);
        }

        public void SignIn(string user, string password)
        {
            UsernameField.Clear();
            UsernameField.SendKeys(user);

            PasswordField.Clear();
            PasswordField.SendKeys(password);

            SubmitButton.Click();
        }
    }
}